//
const { loadTranslation } = require('../utils/translation')
const { getTexts } = require('../components/sayHello')
/**
 * Nacte soubory JSON obsahujici texty v danem jazyce pro pouziti v Layout.svelte
 * @param {string} locale Lokalizace aneb zkratka pouziteho lidskeho jazyka
 */
module.exports = {
  getTexts(locale) {
    const texts = loadTranslation(locale, __dirname)
    texts.sayHello = getTexts(locale)
    return texts
  },
}
