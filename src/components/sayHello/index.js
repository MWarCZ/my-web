const { loadTranslation } = require('../../utils/translation')

module.exports = {
  // SayHello: require('./SayHello.svelte'),
  getTexts(locale) {
    return loadTranslation(locale, __dirname)
  },
}
