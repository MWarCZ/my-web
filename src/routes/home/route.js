const { generatePermalink, generateRequests } = require('../../utils/routes')
const { getTexts: getLayoutTexts } = require('../../layouts')
const { loadTranslation } = require('../../utils/translation')

module.exports = {
  all: async () => {
    return generateRequests([{ slug: '/' }])
  },
  permalink: ({ request }) => {
    return generatePermalink(request.slug, request)
  },
  data: async ({ request }) => {
    const texts = loadTranslation(request.locale, __dirname)
    return {
      texts: {
        ...getLayoutTexts(request.locale),
        ...texts,
      },
    }
  },
}
