//
const { checkArrayOfString } = require('./check')

/**
 * @param {*} texts
 * @example
 * // Example of legit input texts
 * {
 *   "title": "Sample title",
 *   "meta": {
 *     "author": "MWarCZ",
 *     "language": "cs",
 *   }
 *   "long-text": [
 *     "All strings ",
 *     "will be joined ",
 *     "into one string"
 *   ]
 * }
 */
function useTranslation(texts) {
  /**
   * Funkce pro ziskani textu leziciho na konkretni ceste.
   * Funkce se snazi vzdy vratit nejaky text.
   *
   * Pokud na konci cesty neni text, ale ...
   * * __pole textu__: Vsechny texty budou slouceny v jeden text
   * * __cokoliv jineho__: Vraceny text bude odpovidat ceste v hranatych zavorkach (`[ page.xyz ]`)
   * > Pokud `raw=true`, tak je vzdy vracen objekt na konci cesty a nikoliv jen text.
   *
   * @param {string} path Cesta pro ziskani textu. (pr. `article.title`, `page.info.header.content`)
   * @param {boolean} raw Pokud je `true` bude vzdy vracen objekt lezici na konci cesty. Jinak je vzdy vracen text.
   * @returns Vraci text pro zobrazeni nebo objekt lezici na zadane ceste.
   */
  const t = (path = '', raw = false) => {
    let keys = path.split('.')
    let value = texts
    for (let key of keys) {
      value = value[key] || {}
    }
    if (raw) return value
    if (typeof value === 'string') {
      return value
    }
    if (checkArrayOfString(value)) {
      return value.join('')
    }
    return `[ ${path} ]`
  }
  return { t }
}

/**
 * Nacte soubory JSON obsahujici texty v danem jazyce.
 * - Priorita jmen pri nacitani souboru:
 *   1. `texts.<locale>.json`
 *   2. `<locale>.json`
 *   3. `texts.json`
 * @param {string} locale Lokalizace aneb zkratka pouziteho lidskeho jazyka
 * @param {string} directoryPath Cesta k adresari, kde se preklady maji vyskytovat
 */
function loadTranslation(locale, directoryPath) {
  const basename = 'texts'
  const possiblePaths = [
    `${directoryPath}/${basename}.${locale}.json`,
    `${directoryPath}/${locale}.json`,
    `${directoryPath}/${basename}.json`,
  ]
  for (let possiblePath of possiblePaths) {
    try {
      const texts = require(possiblePath)
      return texts || {}
      // eslint-disable-next-line no-empty
    } catch {}
  }
  return {}
}

module.exports = {
  useTranslation,
  loadTranslation,
}
