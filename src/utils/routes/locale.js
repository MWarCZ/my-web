//

const useRouteHelper = require('./routeHelper')

/**
 *
 * @param {string[]} locales
 * @param {string} defaultLocale
 */
function useLocales(locales = ['en'], defaultLocale) {
  return useRouteHelper('locale', locales, defaultLocale)
}

module.exports = useLocales
