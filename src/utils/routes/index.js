///////////////////////////////////////
// Sjednocene nastaveni pro vsechny //
/////////////////////////////////////

const useLocales = require('./locale')
const useThemes = require('./theme')

const locales = ['cs', 'en']
const themes = ['dark', 'dark2', 'light']

const genLocales = useLocales(locales)
const genThemes = useThemes(themes)

const genReq = [genThemes.generateRequests, genLocales.generateRequests]
// Vysledny link bude obracene tj. ve tvaru `/<location>/<theme>/...`
const genLink = [genThemes.generatePermalink, genLocales.generatePermalink]

function generateRequests(requests) {
  let reqs = requests
  for (let gen of genReq) {
    reqs = gen(reqs)
  }
  return reqs
}
function generatePermalink(permalink, request) {
  let link = permalink
  for (let gen of genLink) {
    link = gen(link, request)
  }
  return link
}

module.exports = {
  generateRequests,
  generatePermalink,
  locales,
  themes,
}
