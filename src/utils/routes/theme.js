//

const useRouteHelper = require('./routeHelper')

/**
 *
 * @param {string[]} themes
 * @param {string} defaultTheme
 */
function useThemes(themes, defaultTheme) {
  return useRouteHelper('theme', themes, defaultTheme)
}

module.exports = useThemes
