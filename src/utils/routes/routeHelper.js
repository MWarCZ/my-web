//

const { checkArrayOfString } = require('../check')

/**
 *
 * @param {string} propertyKey Klic/nazev vlastnosti, ktera bude nabývat hodnot.
 * @param {string[]} possibilities Pole možných hodnot pro danou vlastnost.
 * @param {string|undefined} defaultValue
 * Výchozí hodnota pro danou vlastnost (Nebo se pouzije prvni z mozných hodnot).
 */
function useRouteHelper(propertyKey, possibilities, defaultValue) {
  if (
    !(
      Array.isArray(possibilities) &&
      possibilities.length > 0 &&
      checkArrayOfString(possibilities)
    )
  ) {
    possibilities = []
  }
  if (
    typeof defaultValue !== 'string' ||
    !possibilities.includes(defaultValue)
  ) {
    defaultValue = possibilities[0]
  }

  function generateRequests(requests) {
    return requests.reduce((acc, r) => {
      for (let possibility of possibilities) {
        acc.push({ ...r, [propertyKey]: possibility })
      }
      return acc
    }, [])
  }
  /**
   * Add theme prefix.
   * @param {string} permalink Path like `/`, `/path`, or `/path/`.
   * @param {{theme: string}} request
   * @returns {string}
   */
  function generatePermalink(permalink, request) {
    return request[propertyKey] === defaultValue
      ? `${permalink}`
      : `/${request[propertyKey]}${permalink}`
  }

  return {
    generateRequests,
    generatePermalink,
  }
}

module.exports = useRouteHelper
