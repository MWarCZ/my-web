//

function checkArrayOfString(arr) {
  if (!Array.isArray(arr)) return false
  for (let i in arr) {
    if (typeof i !== 'string') return false
  }
  return true
}

module.exports = {
  checkArrayOfString,
}
